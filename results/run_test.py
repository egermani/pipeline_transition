import testing
import sys
sys.path.insert(0, '/gpfswork/rech/gft/umh25bv/pipeline_transition/src/lib')
from utils import dataset as ds
import os
import torch
import importlib
import pandas as pd
from os.path import join as opj
import numpy as np

if __name__ == "__main__":
    
    data_dir='/gpfswork/rech/gft/umh25bv/hcp_many_pipelines_preprocess'
    dpath = '/gpfswork/rech/gft/umh25bv/pipeline_transition'
    model='vox2vox'
    #subset_A='spm-5-0-0'
    #subset_B='fsl-5-0-1'
    vox_loss = 'mse'
    #out_dir=f'{dpath}/data/derived/{subset_A}_to_{subset_B}'
    contrast_list=['right-hand']
    epoch=199
    batch_size=16
    lr=1e-4
    str_lr = "{:.0e}".format(lr)
    dataset_type='paired'

    for subset_A in ['fsl-5-0-0', 'fsl-5-0-1', 'spm-5-0-0', 'spm-5-0-1']:
        for subset_B in ['fsl-5-0-0', 'fsl-5-0-1', 'spm-5-0-0', 'spm-5-0-1']:
            out_dir=f'{dpath}/data/derived/{subset_A}_to_{subset_B}'
            if subset_A != subset_B:
                if dataset_type == 'paired':
                    df_file = f'{dpath}/data/test-dataset_rh_4class-jeanzay.csv'
                    valid_dataset = ds.PairedImageDataset(subset_A, subset_B, df_file, contrast_list)
                    print('Number of images in VALID:', len(valid_dataset.data_B))

                    parameter_file=f"{out_dir}/model-vox2vox_b-{batch_size}_lr-{str_lr}_e-{epoch}_l-{vox_loss}.pt"

                    package = 'models.' + model
                    md = importlib.import_module(package)

                    generator = md.GeneratorUNet()
                    generator.load_state_dict(torch.load(parameter_file, map_location="cpu"))

                    output_dir = f'/gpfswork/rech/gft/umh25bv/pipeline_transition/figures/{subset_A}_to_{subset_B}_b-{batch_size}_lr-{lr}_' + \
                    f'e-{epoch}_model-{model}_l-{vox_loss}'
                    if not os.path.exists(output_dir):
                        os.mkdir(output_dir)
                        
                    testing.transition_metrics(valid_dataset, 
                        generator,
                        output_dir, 
                        expe='valid',
                        src_domain=subset_A, 
                        trg_domain = subset_B)

                    #df = pd.read_csv(opj(output_dir, f'{test}_metrics.csv'))

                if dataset_type == 'unpaired':
                    df_file = f'{dpath}/data/test-dataset_rh_4class-jeanzay.csv'
                    valid_dataset = ds.PairedImageDataset(subset_A, subset_B, df_file, contrast_list)
                    print('Number of images in VALID:', len(valid_dataset.data_B))

                    parameter_file = f"{out_dir}/model-cycleGAN_b-{batch_size}_lr-{str_lr}_e-{epoch}.pt"

                    package = 'models.' + model
                    md = importlib.import_module(package)
                    
                    sys.path.insert(0, '/gpfswork/rech/gft/umh25bv/pipeline_transition/src')
                    cyclegan = torch.load(parameter_file, map_location="cpu")
                    generator = cyclegan.netG_A

                    output_dir = f'/gpfswork/rech/gft/umh25bv/pipeline_transition/figures/{subset_A}_to_{subset_B}_b-{batch_size}_lr-{lr}_' + \
                    f'e-{epoch}_model-{model}'
                    if not os.path.exists(output_dir):
                        os.mkdir(output_dir)

                    testing.transition_metrics(valid_dataset, 
                        generator,
                        output_dir, 
                        expe='valid', 
                        src_domain=subset_A, 
                        trg_domain = subset_B)
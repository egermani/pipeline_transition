import nibabel as nib
import numpy as np
import torch
from nilearn import plotting
import matplotlib.pyplot as plt
from lib import utils

def get_correlation(inim, outim):
    '''
    Compute the Pearson's correlation coefficient between original and reconstructed images.
    '''
    #orig, repro = utils.mask_using_original(inim, outim)
    
    #data1 = orig.get_fdata().copy()
    #data2 = repro.get_fdata().copy()
    
    # Vectorise input data
    data1 = np.reshape(inim, -1)
    data2 = np.reshape(outim, -1)

    in_mask_indices = np.logical_not(
        np.logical_or(
            np.logical_or(np.isnan(data1), np.absolute(data1) == 0),
            np.logical_or(np.isnan(data2), np.absolute(data2) == 0)))

    data1 = data1[in_mask_indices]
    data2 = data2[in_mask_indices]
    
    corr_coeff = np.corrcoef(data1, data2)[0][1]
    
    return corr_coeff

def get_difference(inim, outim):
    '''
    Compute the squared L2 distance (Mean Squared Error) between original and reconstructed images.
    '''
    #orig, repro = utils.mask_using_original(inim, outim)
    
    #data1 = orig.get_fdata().copy()
    #data2 = repro.get_fdata().copy()
    
    # Vectorise input data
    data1 = np.reshape(inim, -1)
    data2 = np.reshape(outim, -1)

    in_mask_indices = np.logical_not(
        np.logical_or(
            np.logical_or(np.isnan(data1), np.absolute(data1) == 0),
            np.logical_or(np.isnan(data2), np.absolute(data2) == 0)))

    data1 = data1[in_mask_indices]
    data2 = data2[in_mask_indices]
    
    diff_array = data1 - data2

    sqr_diff_array = np.square(diff_array)
    
    mse = np.mean(np.array(sqr_diff_array))
    
    return mse


def PSNR(data1, data2):
    mse = get_difference(data1, data2)
    if(mse == 0):  # MSE is zero means no noise is present in the signal .
                  # Therefore PSNR have no importance.
        return 100
    max_pixel = 1
    psnr = 20 * np.log10(max_pixel / np.sqrt(mse))
    return psnr

def calculate_fid(act1, act2):
    # calculate mean and covariance statistics
    mu1, sigma1 = act1.mean(axis=0), cov(act1, rowvar=False)
    mu2, sigma2 = act2.mean(axis=0), cov(act2, rowvar=False)
    # calculate sum squared difference between means
    ssdiff = numpy.sum((mu1 - mu2)**2.0)
    # calculate sqrt of product between cov
    covmean = sqrtm(sigma1.dot(sigma2))
    # check and correct imaginary numbers from sqrt
    if iscomplexobj(covmean):
        covmean = covmean.real
    # calculate score
    fid = ssdiff + trace(sigma1 + sigma2 - 2.0 * covmean)
    return fid

def get_features(model_param, image):
    model = torch.load(model_param, map_location='cpu')

    return features

def FID(model_param, img1, img2):
    features1 = get_features(model_param, img1)
    features2 = get_features(model_param, img2)

    fid = calculate_fid(features1, features2)

    return fid 

def class_change(model_param, image):
    model = torch.load(model_param, map_location='cpu')

    classe = torch.max(model(image), 1)[1]

    return(classe)
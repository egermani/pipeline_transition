import nibabel as nib
import numpy as np
import torch
import matplotlib.pyplot as plt

def recreate_image(img_mat, affine, header, out_img = False):
	'''
	Recreate image based on an matrix, affine and header. 
	'''
	tmp_data = img_mat.detach().cpu().reshape(*img_mat.shape)
	
	if out_img:
		norm_img_data = tmp_data.copy().astype(float)
		norm_img_data = np.nan_to_num(norm_img_data)
		norm_img_data *= 1.0/np.abs(norm_img_data).max()
		img_data = norm_img_data
		
	else:
		img_data = tmp_data

	img = nib.Nifti1Image(img_data, affine=affine, header=header)

	return img

def mask_using_original(inim, outim):
    '''
    Compute the mask of the original map and apply it to the reconstructed one. 
    '''
    # Set masking using NaN's
    data_orig = inim
    data_repro = outim
    
    if np.any(np.isnan(data_orig)):
        data_nan_orig = data_orig
        data_nan_repro = data_repro
        
        data_nan_repro[np.isnan(data_orig)] = np.nan
    else:
        data_nan_orig = data_orig
        data_nan_repro = data_repro

        data_nan_repro[data_orig == 0] = np.nan
        data_nan_orig[data_orig == 0] = np.nan
        
    # Save as image
    data_img_nan_orig = nib.Nifti1Image(data_nan_orig, inim.affine)
    data_img_nan_repro = nib.Nifti1Image(data_nan_repro, outim.affine)

    return data_img_nan_orig, data_img_nan_repro


def find_norm_factor(target):

    target_data = np.nan_to_num(target).get_fdata().copy()
    norm_factor = 1.0/np.abs(target_data).max()

    return norm_factor


def un_normalize(generated, target):

    norm_factor = find_norm_factor(target)

    affine = target.affine
    header = target.header

    generated_un_norm = generated.get_fdata() / norm_factor
    print(norm_factor)

    generated_un_norm = nib.Nifti1Image(generated_un_norm, affine=affine, header=header)

    return generated_un_norm










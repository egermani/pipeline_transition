from lib import metrics, utils
from nilearn import plotting 
import torch
import pandas as pd 
import nibabel as nib
import matplotlib.pyplot as plt
from os.path import join as opj
import numpy as np
from nilearn.plotting.cm import _cmap_d as nilearn_cmaps

def transition_metrics(test_dataset_normalized, generator, output_dir, expe='test', src_domain = 'A', trg_domain='B'):
	'''
	Function to calculate performance metric of a transition model. 

	Parameters:
		- test_dataset: PairedDataset, dataset to test 
		- parameter_file: str, path to the file containing model to test
		- output_dir: str, path to the directory where to store results
		- expe: str, one of 'test', 'test_outcontrast' or 'valid'

	Returns:
		--> Store files in output_dir
	'''
	# Data needed to recreate images
	affine = nib.load(test_dataset_normalized.data_A[1]).affine 
	header = nib.load(test_dataset_normalized.data_A[1]).header

	#generator = torch.load(parameter_file, map_location="cpu") # Load generator
	generator.eval()

	test_loader_norm = torch.utils.data.DataLoader(test_dataset_normalized, batch_size=1) # Load dataset 
	#test_loader_res = torch.utils.data.DataLoader(test_dataset_rescaled, batch_size=1) # Load dataset 

	# Dictionnary which will contain the metrics
	correlations = {'source-generated':[], 'target-generated':[], 'source-target':[]}
	mse = {'source-generated':[], 'target-generated':[], 'source-target':[]}
	psnr = {'source-generated':[], 'target-generated':[], 'source-target':[]}
	fid = {'source-generated':[], 'target-generated':[], 'source-target':[]}
	classe = {'source':[], 'target':[], 'generated':[]}

	device= 'cpu'
	with torch.no_grad():
		for i, data in enumerate(test_loader_norm):
			imgs = data[0].float().to(device) # Original source image 
			out = generator(imgs).cpu() # Translated image
			
			# Figure for plotting
			# f = plt.figure(figsize = (10, 10))
			# gs = f.add_gridspec(3, 1)
			# ax0 = f.add_subplot(gs[0, 0])
			# ax1 = f.add_subplot(gs[1, 0])
			# ax2 = f.add_subplot(gs[2, 0])

			source_img = data[0].float().cpu()
			target_img = data[1].float().cpu()
			generated_img = out.float() * np.array(target_img != 0).astype('float')
			#generated_img[np.logical_or(np.isnan(target_img), np.absolute(target_img) == 0)] = 0

			#generated_img = utils.un_normalize(generated_img, target_img)
			nib.save(nib.Nifti1Image(source_img, affine, header), opj(output_dir, f'src-image-{i}_src-{src_domain}_trg-{trg_domain}.nii.gz'))
			nib.save(nib.Nifti1Image(target_img, affine, header), opj(output_dir, f'trg-image-{i}_src-{src_domain}_trg-{trg_domain}.nii.gz'))
			nib.save(nib.Nifti1Image(generated_img, affine, header), opj(output_dir, f'gen-image-{i}_src-{src_domain}_trg-{trg_domain}.nii.gz'))

			corr_source_generated = metrics.get_correlation(source_img, generated_img)
			corr_target_generated = metrics.get_correlation(target_img, generated_img)
			corr_source_target = metrics.get_correlation(source_img, target_img)
			
			correlations['source-target'].append(corr_source_target)
			correlations['source-generated'].append(corr_source_generated)
			correlations['target-generated'].append(corr_target_generated)

			print('Corr Source-Generated', corr_source_generated)
			print('Corr Target-Generated', corr_target_generated)
			print('Corr Source-Target', corr_source_target)
			
			mse_source_generated = metrics.get_difference(source_img, generated_img)
			mse_target_generated = metrics.get_difference(target_img, generated_img)
			mse_source_target = metrics.get_difference(source_img, target_img)
			
			mse['source-target'].append(mse_source_target)
			mse['source-generated'].append(mse_source_generated)
			mse['target-generated'].append(mse_target_generated)

			print('MSE Source-Generated', mse_source_generated)
			print('MSE Target-Generated', mse_target_generated)
			print('MSE Source-Target', mse_source_target)

			psnr_source_generated = metrics.PSNR(source_img, generated_img)
			psnr_target_generated = metrics.PSNR(target_img, generated_img)
			psnr_source_target = metrics.PSNR(source_img, target_img)
			
			psnr['source-target'].append(psnr_source_target)
			psnr['source-generated'].append(psnr_source_generated)
			psnr['target-generated'].append(psnr_target_generated)

			print('psnr Source-Generated', psnr_source_generated)
			print('psnr Target-Generated', psnr_target_generated)
			print('psnr Source-Target', psnr_source_target)

			model_param = '/gpfswork/rech/gft/umh25bv/pipeline_classification/data/derived/model_b-64_lr-1e-04_epochs_190.pt'

			classe_source = metrics.class_change(model_param, source_img)
			classe_target = metrics.class_change(model_param, target_img)
			classe_generated = metrics.class_change(model_param, target_img)
			print(classe_source, classe_target, classe_generated)

			classe['source'].append(classe_source)
			classe['target'].append(classe_target)
			classe['generated'].append(classe_generated)

			#fid_source_generated = metrics.FID(model_param, source_img, generated_img)
			#fid_target_generated = metrics.FID(model_param, target_img, generated_img)
			#fid_source_target = metrics.FID(model_param, source_img, target_img)
			
			#fid['source-target'].append(fid_source_target)
			#fid['source-generated'].append(fid_source_generated)
			#fid['target-generated'].append(fid_target_generated)

			#print('fid Source-Generated', fid_source_generated)
			#print('fid Target-Generated', fid_target_generated)
			#print('fid Source-Target', fid_source_target)

			# plotting.plot_glass_brain(nib.Nifti1Image(np.array(source_img)[0,0,:,:,:], affine), figure=f, axes=ax0, 
			# 					   colorbar = False, annotate=False, cmap=nilearn_cmaps['cold_hot'], plot_abs=False,  title='Source')
			# plotting.plot_glass_brain(nib.Nifti1Image(np.array(target_img)[0,0,:,:,:], affine), figure=f, axes=ax1, 
			# 					 colorbar = False, annotate=False, cmap=nilearn_cmaps['cold_hot'], plot_abs=False, title= 'Target')
			# plotting.plot_glass_brain(nib.Nifti1Image(np.array(generated_img)[0,0,:,:,:], affine), figure=f, axes=ax2, colorbar = False, annotate=False, cmap=nilearn_cmaps['cold_hot'], plot_abs=False, title='Generated')
			# plt.savefig(opj(output_dir, f'transition_img_{test_dataset_normalized.data[i]}_{expe}.png'))
			# plt.show()

	df = pd.DataFrame()
	df['Correlations S-G']=correlations['source-generated']
	df['Correlations S-T']=correlations['source-target']
	df['Correlations T-G']=correlations['target-generated']
	df['MSE S-G']=mse['source-generated']
	df['MSE S-T']=mse['source-target']
	df['MSE T-G']=mse['target-generated']
	df['psnr S-G']=psnr['source-generated']
	df['psnr S-T']=psnr['source-target']
	df['psnr T-G']=psnr['target-generated']
	#df['fid S-G']=fid['source-generated']
	#df['fid S-T']=fid['source-target']
	#df['fid T-G']=fid['target-generated']
	df['classe S'] = classe['source']
	df['classe T'] = classe['target']
	df['classe G'] = classe['generated']

	df.to_csv(opj(output_dir, f'{expe}_metrics.csv'))

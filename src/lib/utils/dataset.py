from torch.utils.data import Dataset
from glob import glob
from os.path import join as opj
from torch import default_generator, randperm, Generator
from torch._utils import _accumulate
import nibabel as nib
import numpy as np
import torch
import json
import random
import os 
import shutil
import pandas as pd

class PairedImageDataset(Dataset):
    '''
    Create a Dataset object used to load training data and train the autoencoder (no labels needed).

    Parameters:
        - data_dir_A, str: directory where images of dataset A are stored
        - data_dir_B, str: directory where images of dataset B are stored
        - id_file, str: path to the text file containing ids of images of interest

    Attributes:
        - data_A, list of str: list containing all paths to images of the dataset A 
        - data_B, list of str: list containing all paths to images of the dataset B
        - ids, list of int: list containing all ids of images of the selected dataset
        - data, list of int: list containing all ids of images of the selected dataset
    '''
    def __init__(self, subset_A, subset_B, df_file, contrast_list):
        self.df = pd.read_csv(df_file)

        self.df_A = self.df[self.df['pipelines']==subset_A]
        self.df_B = self.df[self.df['pipelines']==subset_B]

        if contrast_list != 'All':
            self.df_A = self.df_A[self.df_A['contrast'].isin(contrast_list)]
            self.df_B = self.df_B[self.df_B['contrast'].isin(contrast_list)]
        
        self.data_A = self.df_A['filepaths'].tolist()
        self.data_B = self.df_B['filepaths'].tolist()

        self.ids = sorted(self.df_A['groups'].tolist())
        self.data = self.ids

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        '''
        Returns a pair of sample [data_A[idx], data_B[idx]] as Pytorch Tensors.
        '''
        fname_A = self.data_A[idx]
        sample_A = nib.load(fname_A).get_fdata().copy().astype(float)
        sample_A = np.nan_to_num(sample_A)
        sample_A = torch.tensor(sample_A).view((1), *sample_A.shape)

        fname_B = self.data_B[idx]
        sample_B = nib.load(fname_B).get_fdata().copy().astype(float)
        sample_B = np.nan_to_num(sample_B)
        sample_B = torch.tensor(sample_B).view((1), *sample_B.shape)
        
        return sample_A, sample_B

    def get_original_ids(self):
        return self.ids
    
    
class UnpairedImageDataset(Dataset):
    '''
    Create a Dataset object used to load training data and train the autoencoder (no labels needed).

    Parameters:
        - data_dir_A, str: directory where images of dataset A are stored
        - data_dir_B, str: directory where images of dataset B are stored
        - id_file, str: path to the text file containing ids of images of interest

    Attributes:
        - data_A, list of str: list containing all paths to images of the dataset A 
        - data_B, list of str: list containing all paths to images of the dataset B
        - ids, list of int: list containing all ids of images of the selected dataset
        - data, list of int: list containing all ids of images of the selected dataset
    '''
    def __init__(self, subset_A, subset_B, df_file, contrast_list):
        self.df = pd.read_csv(df_file)

        self.df_A = self.df[self.df['pipelines']==subset_A]
        self.df_B = self.df[self.df['pipelines']==subset_B]

        if contrast_list != 'All':
            self.df_A = self.df_A[self.df_A['contrast'].isin(contrast_list)]
            self.df_B = self.df_B[self.df_B['contrast'].isin(contrast_list)]
        
        self.data_A = self.df_A['filepaths'].tolist()
        self.data_B = self.df_B['filepaths'].tolist()

        self.ids = sorted(self.df_A['groups'].tolist())
        self.data = self.ids

        random.shuffle(self.data_A)
        random.shuffle(self.data_B)

        assert(len(self.data_A)==len(self.data_B))
        
        self.data = [(self.data_A[i], self.data_B[i]) for i in range(len(self.data_A))]

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        '''
        Returns a pair of sample [data_A[idx], data_B[idx]] as Pytorch Tensors.
        '''
        fname_A = self.data_A[idx]
        sample_A = nib.load(fname_A).get_fdata().copy().astype(float)
        sample_A = np.nan_to_num(sample_A)
        sample_A = sample_A[0:48,0:56,0:48]
        sample_A = torch.tensor(sample_A).view((1), *sample_A.shape)

        fname_B = self.data_B[idx]
        sample_B = nib.load(fname_B).get_fdata().copy().astype(float)
        sample_B = np.nan_to_num(sample_B)
        sample_B = sample_B[0:48,0:56,0:48]
        sample_B = torch.tensor(sample_B).view((1), *sample_B.shape)
        
        return sample_A, sample_B

    def get_original_ids(self):
        return self.data_A, self.data_B

from lib.utils import preprocessing

if __name__ == '__main__':
	data_dir = '/Users/egermani/Documents/pipeline_distance/data/group*right-foot*tstat.nii*'
	output_dir = '/Volumes/hcp_many_pipelines_preprocess'
	
	preprocessing.preprocessing(data_dir, output_dir)
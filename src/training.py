# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)
simplefilter(action='ignore', category=UserWarning)
simplefilter(action='ignore', category=RuntimeWarning)

# To test
from lib.utils import dataset as ds 
from lib import vox2vox_trainer, cycleGAN_trainer
import os
from os.path import join as opj
import sys
import pickle
import importlib

def main():
    #### PARAMETERS
    data_dir='/gpfswork/rech/gft/umh25bv/hcp_many_pipelines_preprocess'
    dpath = '/gpfswork/rech/gft/umh25bv/pipeline_transition'
    b=0.5
    ngpu=1
    model='cycleGAN'
    r='lr'
    #subset_A='spm-5-0-0'
    #subset_B='spm-5-0-1'
    contrast_list=['right-hand']
    epochs=200
    batch_size=16
    lr=1e-4
    dataset_type='unpaired'
    seed=42
    vox_loss = 'mse'
    
    for subset_A in ['fsl-5-0-0']:
        for subset_B in ['fsl-5-0-0', 'fsl-5-0-1', 'spm-5-0-0', 'spm-5-0-1']:
            out_dir=f'{dpath}/data/derived/{subset_A}_to_{subset_B}'
            if subset_A != subset_B:
                if dataset_type == 'paired':
                    df_file = f'{dpath}/data/train-dataset_rh_4class-jeanzay.csv'
                    train_dataset = ds.PairedImageDataset(f'{data_dir}/normalized', subset_A, subset_B, df_file, contrast_list)
                    print('Number of images in TRAIN:', len(train_dataset.data_B))

                    df_file = f'{dpath}/data/test-dataset_rh_4class-jeanzay.csv'
                    valid_dataset = ds.PairedImageDataset(f'{data_dir}/normalized', subset_A, subset_B, df_file, contrast_list)
                    print('Number of images in VALID:', len(valid_dataset.data_B))
                
                elif dataset_type == 'unpaired':
                    df_file = f'{dpath}/data/train-dataset_rh_4class-jeanzay.csv'
                    train_dataset = ds.UnpairedImageDataset(subset_A, subset_B, df_file, contrast_list)
                    print('Number of images in TRAIN:', len(train_dataset.data))

                    valid_dataset = ds.UnpairedImageDataset(subset_A, subset_B, df_file, contrast_list)
                    print('Number of images in VALID:', len(valid_dataset.data))
                
                package = 'lib.models.' + model
                md = importlib.import_module(package)

                if model == 'vox2vox':
                    vox2vox_trainer.trainer(train_dataset, valid_dataset, md, lr, batch_size, epochs, vox_loss, ngpu, seed, out_dir)

                elif model == 'cycleGAN':
                    cycleGAN_trainer.trainer(train_dataset, valid_dataset, md, lr, batch_size, epochs, seed, out_dir)

if __name__ == "__main__":
    main()







